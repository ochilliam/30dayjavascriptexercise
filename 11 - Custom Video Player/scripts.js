/* Get Our Elements */
const player = document.querySelector('.player');
const video = player.querySelector('.viewer');
const progress = player.querySelector('.progress');
const progressBar = player.querySelector('.progress__filled');
const toggle = player.querySelector('.toggle');
const skipButtons = player.querySelectorAll('[data-skip]');
const ranges = player.querySelectorAll('.player__slider');

var isActive = false
var mousedown = false


/* Build out functions */



function togglePlay() {
    var method = video.paused ? 'play' : 'pause'
    video[method]()
}

function onUpdateBtn() {
    var icon = video.paused ? '►' : '❚ ❚'
    toggle.textContent = icon
}

function skip() {
    video.currentTime += parseFloat(this.dataset.skip)    
}

function onRangeChange(){
    if (!isActive) {
        return
    }
    video[this.name] = this.value
}

function handleProgressBar() {
    let percent = ( video.currentTime / video.duration) * 100
    progressBar.style.flexBasis = `${percent}%`
}

function scrub(e) {
    if (!mousedown) {
        return
    }
    let scrubTime = (e.offsetX / progress.offsetWidth)  * video.duration
    video.currentTime = scrubTime
    
}

/* Hook up the event listeners */

video.addEventListener('click', togglePlay)
video.addEventListener('play', onUpdateBtn)
video.addEventListener('pause', onUpdateBtn)
toggle.addEventListener('click', togglePlay)
video.addEventListener('timeupdate', handleProgressBar)
progress.addEventListener('mousemove', (e)=>{ mousedown &&  scrub(e)})
progress.addEventListener('mousedown', () => { mousedown =! mousedown })
progress.addEventListener('mouseup', ()=> mousedown = false)
skipButtons.forEach((btn)=>{
    btn.addEventListener('click', skip)
})
ranges.forEach((range)=>{
    range.addEventListener('change', onRangeChange)
    range.addEventListener('mousemove', onRangeChange)
    range.addEventListener('mouseout', ()=>{ isActive = false})
    range.addEventListener('mouseup', ()=>{ isActive = false})
    range.addEventListener('mousedown', ()=>{ isActive = !isActive})
})